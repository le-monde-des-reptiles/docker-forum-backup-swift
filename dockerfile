FROM centos:7

RUN yum install python-setuptools -y \
&& easy_install pip \
&& pip install python-swiftclient python-keystoneclient
ADD run-backup.sh /opt
RUN chmod +x /opt/run-backup.sh

ENTRYPOINT [ "/opt/run-backup.sh" ]