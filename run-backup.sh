#!/bin/bash

set -x
DATE=`date +%F`

mkdir /mnt/backup/
tar -zcvf /mnt/backup/$DATE-forum.tar --exclude .git /home/forum
swift upload $BUCKET /mnt/backup/$DATE-forum.tar --object-name $DATE-forum.tar
swift post $BUCKET $DATE-forum.tar -H "X-Delete-After:1209600"
rm -rf /mnt/backup/$DATE-forum.tar